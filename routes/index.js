var express = require('express');
var router = express.Router();
var path = require('path');
/* GET home page. */
router.get('/', function(req, res, next) {
  res.redirect(301, '/bigscreen');
});
router.get('/bigscreen', function(req, res, next) {
  res.sendFile(path.join(__dirname, '../public/pages/bigscreen.html'));
});

module.exports = router;
