# 说明
该项目将传统的html页面进行拆分，根据配置打包成多页。
page-config.js的components数组包含的组件配置中，所有script被合并成一个以page命名的js；所有style.less 会被合并成以page命名的css

开发模式下ejs由express渲染，打包时通过gulp-ejs打包成html或其他文件。

# 安装

```
npm install
```

# 开发模式

用node作为服务
```
npm run serve
```
它只是不跑node服务，而只用其它(如java)作为服务，其它都一样
```
npm run serve:other
```
// 最终打包输入，先生成less,js,html, 然后拷贝到发布目录
```
npm run build
```
# 其它
```
// 合并样式文件
gulp less

// 合并js文件
gulp js

// ejs模板生成页面文件
gulp ejs

// 拷贝publishSource静态文件复制到publishPath的目录
gulp static

```

# gulp生成配置
page-config.js

```
// 文件生成的目录，如果使用本项目的node服务开发，可以直接生成到public,须在routes中配置路由
const tempStatic = 'public'
// 文件生成的目录，例如生成到java项目中，此时需要设置代理地址
const javaStatic = 'javaproject'
// 改配置用于控制js,css,的处理与合并，拷贝
module.exports = {
    // browser-sync 的代理地址，可实现热更新
    serverProxy: 'http://localhost:3000',
    tempStatic,
    javaStatic,
    // 发布相关
    publishPath: 'dist/',
    // 静态文件地址
    publishSource: [
        `${tempStatic}/**/*.*`,
    ],
    // 文件如果没有配置output，则默认放在此位置
    defaultOutput: './public',
    pages: [
        {
            components: [{
                base: 'src/views/bigscreen',
                // 默认会合并目录下所有的js,less
                blocks: ['left', 'right']
            }, {
                base: 'src/components/common',
                blocks: ['header', 'bottom']
            }, {
                base: 'src/components',
                blocks: [
                    {
                        name: 'utils',
                        // 如果想指定合并的顺序，可以这么写。如果没有指定，会按默认顺序合并。
                        files: {
                            js: ['event', 'resize']
                        }
                    }
                ]
            }],
            page: 'bigscreen',
            entryFolder: 'src/views/bigscreen',
            entryFile: {
                dev: 'htmlPage',
                prod: 'jspPage'
            },
             // files 用来定义合并顺序
             files: {
                js: ['main']
            },
            output: {
                js: `/js`,
                less: `/css`,
                ejs: `/pages`
            },
            ejsFormat: '.html'
        }
    ]
}
```
