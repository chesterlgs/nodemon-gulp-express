<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="../css/bigscreen.css"/>
</head>
<body>
    <div>hello bigscreen</div>
    <div class="border header">header</div>
    <div class="border left">left</div>
    <div class="border right">right</div>
    <div class="border bottom">bottom</div>
    <script src="../js/bigscreen.js"></script>
</body>
</html>