
// 文件生成的目录，如果使用本项目的node服务开发，可以直接生成到public,须在routes中配置路由
const tempStatic = 'public'
// 文件生成的目录，例如生成到java项目中，此时需要设置代理地址
const javaStatic = 'javaproject'
// 改配置用于控制js,css,的处理与合并，拷贝
module.exports = {
    // browser-sync 的代理地址，可实现热更新
    serverProxy: 'http://localhost:3000',
    tempStatic,
    javaStatic,
    // 发布相关
    publishPath: 'dist/',
    // 静态文件地址
    publishSource: [
        `${tempStatic}/**/*.*`,
    ],
    // 文件如果没有配置output，则默认放在此位置
    defaultOutput: './public',
    pages: [
        {
            components: [{
                base: 'src/views/bigscreen',
                blocks: ['left', 'right']
            }, {
                base: 'src/components/common',
                blocks: ['header', 'bottom']
            }],
            page: 'bigscreen',
            entryFolder: 'src/views/bigscreen',
            entryFile: {
                dev: 'htmlPage',
                prod: 'jspPage'
            },
             // files 用来定义合并顺序
             files: {
                js: ['main']
            },
            output: {
                js: `/js`,
                less: `/css`,
                ejs: `/pages`
            },
            ejsFormat: '.html'
        }
    ]
}